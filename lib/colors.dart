import "package:flutter/material.dart";

const PRIMARY = Color.fromRGBO(233, 30, 99, 1);
const GREEN = Color.fromRGBO(78, 229, 139, 1);
const YELLOW = Color.fromRGBO(255, 255, 51, 1);
const BLACK = Color.fromRGBO(35, 31, 32, 1);
const WHITE = Colors.white;
