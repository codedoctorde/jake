import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';

import 'main.dart';
import 'package:flutter/material.dart';
import 'colors.dart' as colors;

class JakeDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          ListTile(
            title: Text('Home'),
            leading: Icon(MdiIcons.home),
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => HomePage()),
              );
            },
          ),

        ],
      ),
    );
  }
}